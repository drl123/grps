defmodule Grps.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Grps.Accounts` context.
  """

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        id: Faker.UUID.v4(),
        name: Faker.Person.name(),
        role: "member",
        email: Faker.Internet.email(),
        avatar_url: Faker.Internet.url(),
        slug: Faker.Internet.slug()
      })
      |> Grps.Accounts.create_user()

    user
  end
end
