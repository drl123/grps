defmodule Grps.DiscussionsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Grps.Discussions` context.
  """

  @doc """
  Generate a post.
  """
  def post_fixture(attrs \\ %{}) do
    {:ok, post} =
      attrs
      |> Enum.into(%{
        body: "some body",
        pinned_at: ~U[2022-01-20 22:37:00.000000Z]
      })
      |> Grps.Discussions.create_post()

    post
  end
end
