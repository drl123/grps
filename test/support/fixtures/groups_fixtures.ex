defmodule Grps.GroupsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Grps.Groups` context.
  """

  alias Grps.Groups.Membership
  alias Grps.Repo

  @doc """
  Generate a group.
  """
  def group_fixture(attrs \\ %{}) do
    user_id = attrs[:user_id] || Grps.AccountsFixtures.user_fixture().id
    {:ok, group} =
      attrs
      |> Enum.into(%{
        user_id: user_id,
        cover_url: Faker.Internet.url(),
        deleted_at: nil,
        description: Faker.String.base64(150),
        name: Faker.Person.name(),
        share_token: Nanoid.generate()
      })
      |> Grps.Groups.create_group(user_id)

    group
  end

  def create_group_with_member(user_id, attrs \\ %{}) do
    group = group_fixture(attrs)

    %Membership{user_id: user_id, group_id: group.id, joined_at: DateTime.utc_now()}
    |> Repo.insert()

    group
  end
end
