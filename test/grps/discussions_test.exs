defmodule Grps.DiscussionsTest do
  use Grps.DataCase

  import Grps.{AccountsFixtures, GroupsFixtures}

  alias Grps.{Discussions, Repo}
  alias Grps.Discussions.Post
  alias Grps.Groups.Membership

  describe "list_posts_for_group/2" do
    setup do
      group = group_fixture(name: "My Group") |> Repo.preload(:owner)
      posts = for id <- 1..11 do
                {:ok, post} = %Post{user_id: group.user_id, group_id: group.id, body: "Post No. #{id}"} |> Repo.insert()
                post
              end
      last = List.last(posts)
      {:ok, _reply} = %Post{user_id: group.user_id, group_id: group.id, body: "reply", parent_id: last.id} |> Repo.insert()

      other_group = group_fixture(name: "My Group") |> Repo.preload(:owner)
      {:ok, _other} = %Post{group_id: other_group.id, user_id: other_group.owner.id, body: "Some body"}  |> Repo.insert()

      {:ok, group: group, posts: posts}
    end

    test "lists the last 10 posts for the group latest-first", %{group: group, posts: posts} do
      selected_posts = Enum.take(posts, -10) |> Enum.reverse()
      assert Discussions.list_posts_for_group(group.id) == selected_posts
    end

    test "lists the last n posts for the group latest-first when passed a limit of n", %{group: group, posts: posts} do
      selected_posts = Enum.take(posts, -3) |> Enum.reverse()
      assert Discussions.list_posts_for_group(group.id, limit: 3) == selected_posts
    end

    test "lists the last 10 posts newer than the date passed as newer_than newest_first", %{group: group, posts: posts} do
      current_post = Enum.at(posts, 4)
      selected_posts = Enum.slice(posts, 5..11) |> Enum.reverse()
      assert Discussions.list_posts_for_group(group.id, newer_than: current_post.created_at) == selected_posts
    end

    test "lists the last 10 posts older than the date passed as older_than newest-first", %{group: group, posts: posts} do
      current_post = Enum.at(posts, 4)
      selected_posts = Enum.slice(posts, 0, 4) |> Enum.reverse()
      assert Discussions.list_posts_for_group(group.id, older_than: current_post.created_at) == selected_posts
    end

    test "lists the posts around the post id, with even limit", %{group: group, posts: posts} do
      current_post = Enum.at(posts, 4)
      selected_posts = Enum.slice(posts, 2..6) |> Enum.reverse()
      assert Discussions.list_posts_for_group(group.id, around: current_post.id, limit: 4) == selected_posts
    end

    test "lists the posts around the post id, with odd limit", %{group: group, posts: posts} do
      current_post = Enum.at(posts, 4)
      selected_posts = Enum.slice(posts, 3..5) |> Enum.reverse()
      assert Discussions.list_posts_for_group(group.id, around: current_post.id, limit: 3) == selected_posts
    end

    test "lists the posts around the post id, when cannot be centered, clips one end", %{group: group, posts: posts} do
      current_post = Enum.at(posts, 10)
      selected_posts = Enum.slice(posts, 5..11) |> Enum.reverse()
      assert Discussions.list_posts_for_group(group.id, around: current_post.id, limit: 10) == selected_posts
    end
  end

  describe "create_post/2" do
    setup do
      group = group_fixture()
      user = user_fixture()
      Repo.insert(%Membership{user_id: user.id, group_id: group.id, joined_at: DateTime.utc_now()})

      {:ok, group: group, user: user}
    end

    test "creates a new post in the group with valid attrs", %{group: group, user: user} do
      valid_attrs = %{user_id: user.id, group_id: group.id, body: "My first post!"}
      assert {:ok, %Post{} = post } = Discussions.create_post(valid_attrs, user.id)
      assert post.user_id == user.id
      assert post.group_id == group.id
      assert post.body == valid_attrs[:body]
    end

    test "returns and error tuple and changeset with invalid attrs", %{group: group, user: user} do
      invalid_attrs = %{user_id: user.id, group_id: group.id, body: ""}
      assert {:error, %Ecto.Changeset{}} = Discussions.create_post(invalid_attrs, user.id)
    end

    test "returns {:error, :not_authorized} when posted as a non-member", %{group: group} do
      non_member = user_fixture()
      valid_attrs = %{user_id: non_member.id, group_id: group.id, body: "My first post!"}
      assert {:error, :not_authorized} = Discussions.create_post(valid_attrs, non_member.id)
    end

    test "returns {:error, :not_authorized} if the post user and current user do not match",  %{group: group, user: user} do
      valid_attrs = %{user_id: user.id, group_id: group.id, body: "My first post!"}
      assert {:error, :not_authorized} = Discussions.create_post(valid_attrs, group.user_id)
    end

    test "returns {:error, :not_authorized} if the group_id does not exist", %{user: user} do
      valid_attrs = %{user_id: user.id, group_id: Ecto.UUID.generate(), body: "My first post!"}
      assert {:error, :not_authorized} = Discussions.create_post(valid_attrs, user.id)
    end
  end

  describe "update_post/3" do
    setup do
      group = group_fixture()
      user = user_fixture()
      Repo.insert(%Membership{user_id: user.id, group_id: group.id, joined_at: DateTime.utc_now()})

      {:ok, post} = Discussions.create_post(%{user_id: user.id, group_id: group.id, body: "foo"}, user.id)

      {:ok, group: group, user: user, post: post}
    end

    test "updates the post with valid attrs and the post owner as current user", %{post: post} do
      valid_attrs = %{body: "foo bar"}
      assert {:ok, %Post{} = updated_post} = Discussions.update_post(post, valid_attrs, post.user_id)
      assert updated_post.body == "foo bar"
    end

    test "updates the post with valid attrs and the group admin as current user", %{post: post, group: group} do
      valid_attrs = %{body: "foo bar"}
      assert {:ok, %Post{} = updated_post} = Discussions.update_post(post, valid_attrs, group.user_id)
      assert updated_post.body == "foo bar"
      assert updated_post.user_id == post.user_id
    end

    test "updates an error tuple with a changeset with invalid attrs", %{post: post} do
      invalid_attrs = %{body: ""}
      assert {:error, %Ecto.Changeset{}} = Discussions.update_post(post, invalid_attrs, post.user_id)
    end

    test "returns {:error, :not_authorized} if the user does not match and not an admin", %{post: post, group: group} do
      other_user = user_fixture()
      Repo.insert(%Membership{user_id: other_user.id, group_id: group.id, joined_at: DateTime.utc_now()})
      valid_attrs = %{body: "foo bar"}
      assert {:error, :not_authorized} == Discussions.update_post(post, valid_attrs, other_user.id)
    end

    test "it ignores changing the user_id when updating the post even if changed", %{post: post, group: group} do
      valid_attrs = %{body: "foo bar", user_id: group.user_id}
      assert {:ok, %Post{} = updated_post} = Discussions.update_post(post, valid_attrs, group.user_id)
      assert updated_post.body == "foo bar"
      assert updated_post.user_id == post.user_id
    end

    test "it ignores changing the group_id when updating the post even if changed", %{post: post, group: group} do
      other_group = group_fixture(user_id: group.user_id)
      valid_attrs = %{body: "foo bar", group_id: other_group.id}
      assert {:ok, %Post{} = updated_post} = Discussions.update_post(post, valid_attrs, other_group.user_id)
      assert updated_post.body == "foo bar"
      assert updated_post.group_id == post.group_id
    end
  end

  describe "delete_post/2" do
    setup do
      group = group_fixture()
      user = user_fixture()
      Repo.insert(%Membership{user_id: user.id, group_id: group.id, joined_at: DateTime.utc_now()})

      {:ok, post} = Discussions.create_post(%{user_id: user.id, group_id: group.id, body: "foo"}, user.id)

      {:ok, group: group, user: user, post: post}
    end

    test "allows the posting user to delete their post", %{post: post} do
      assert {:ok, %Post{}} = Discussions.delete_post(post.id, post.user_id)
      assert is_nil(Repo.get(Post, post.id))
    end

    test "allows the group admin to delete the post", %{post: post, group: group} do
      assert {:ok, %Post{}} = Discussions.delete_post(post.id, group.user_id)
      assert is_nil(Repo.get(Post, post.id))
    end

    test "returns {:error, :not_authorized} if not a group member", %{post: post} do
      other_user = user_fixture()
      assert {:error, :not_authorized} = Discussions.delete_post(post.id, other_user.id)
      refute is_nil(Repo.get(Post, post.id))
    end

    test "returns {:error, :not_authorized} if a group member but not post owner", %{post: post, group: group} do
      other_user = user_fixture()
      Repo.insert(%Membership{user_id: other_user.id, group_id: group.id, joined_at: DateTime.utc_now()})
      assert {:error, :not_authorized} = Discussions.delete_post(post.id, other_user.id)
      refute is_nil(Repo.get(Post, post.id))
    end

    test "returns {:error, :not_found} if the post does not exist", %{group: group} do
      assert {:error, :not_found} = Discussions.delete_post(Ecto.UUID.generate(), group.user_id)
    end
  end
end
