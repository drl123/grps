defmodule Grps.Accounts.CoreUserTest do
  use Grps.DataCase

  alias Grps.Accounts.CoreUser
  alias Grps.Accounts.CoreUser.CoreFake

  setup do
    core_user = CoreFake.core_user()
    {:ok, core_user: core_user}
  end

  describe "authenticate/2" do
    test "authentication with valid credentials succeeds", %{core_user: core_user} do
      authenticated_user = CoreUser.authenticate("user@forever.com", "Password1")
      assert authenticated_user === core_user
    end

    test "authentication with invalid credentials returns nil" do
      assert CoreUser.authenticate("user@forever.com", "not my password") === nil
    end
  end

  describe "find_by_token/1" do
    test "returns the user when a valid auth_token", %{core_user: core_user} do
      assert CoreUser.find_by_token("valid_auth_token") === core_user
    end

    test "returns nil if the auth_token is not valid" do
      assert CoreUser.find_by_token("not_my_token") === nil
    end
  end

  describe "load_core_user/1" do
    test "loads the Core User from the Core App" do
      user_id = Ecto.UUID.generate()
      assert %CoreUser{} = core_user = CoreUser.load_core_user(user_id)
      assert core_user.id == user_id
      refute is_nil(core_user.name)
    end

    test "returns nil if the Core User is not found" do
      user_id = "a-bad-user-id"
      assert is_nil(CoreUser.load_core_user(user_id))
    end
  end
end
