defmodule Grps.AccountsTest do
  use Grps.DataCase

  alias Grps.{Accounts}
  alias Grps.Accounts.{CoreUser, User}

  setup do
    external_user = CoreUser.core_user()
    {:ok, external_user: external_user}
  end

  describe "authenticating users" do
    test "authenticate/2 returns the core user when authentication succeeds", %{external_user: external_user} do
      (%CoreUser{} = core_user) = Accounts.authenticate("user@forever.com", "Password1")
      assert core_user.name == external_user.name
      assert core_user.slug == external_user.slug
      assert core_user.email == external_user.email
      assert core_user.id == external_user.id
      assert core_user.avatar_url == external_user.avatar_url
      assert core_user.auth_token == external_user.auth_token
    end

    test "authenticate/2 updates the user when the authentication succeeds and the user exists", %{external_user: external_user} do
      {:ok, user} = Accounts.create_user(%{Map.from_struct(external_user) | name: "someone else"})
      Accounts.authenticate("user@forever.com", "Password1")

      updated_user = Accounts.get_user!(user.id)
      assert updated_user.name == "Mister User"
    end

    test "authenticate/2 creates the user when the authentication succeeds and the user does not exist", %{external_user: external_user} do
      Accounts.authenticate("user@forever.com", "Password1")

      new_user = Accounts.get_user!(external_user.id)
      assert new_user.name == "Mister User"
    end

    test "authenticate/2 returns nil and does not create the user when the authentication fails", %{external_user: external_user} do
      assert nil == Accounts.authenticate("user@forever.com", "Password2")
      assert_raise Ecto.NoResultsError, fn ->
        Accounts.get_user!(external_user.id)
      end
    end


    test "find_by_token/1 returns the core user when the token is valid", %{external_user: external_user} do
      (%CoreUser{} = core_user) = Accounts.find_by_token("valid_auth_token")
      assert core_user.name == external_user.name
      assert core_user.slug == external_user.slug
      assert core_user.email == external_user.email
      assert core_user.id == external_user.id
      assert core_user.avatar_url == external_user.avatar_url
      assert core_user.auth_token == external_user.auth_token

      user = Accounts.get_user!(core_user.id)
      assert user.id == core_user.id
    end

    test "find_by_token/1 updates the user when the auth_token is valid and the user exists", %{external_user: external_user} do
      {:ok, user} = Accounts.create_user(%{Map.from_struct(external_user) | name: "someone else"})
      Accounts.find_by_token("valid_auth_token")

      updated_user = Accounts.get_user!(user.id)
      assert updated_user.name == "Mister User"
    end

    test "find_by_token/1 creates the user when the auth_token is valid and the user does not exist", %{external_user: external_user} do
      Accounts.find_by_token("valid_auth_token")

      new_user = Accounts.get_user!(external_user.id)
      assert new_user.name == "Mister User"
    end

    test "find_by_token/1 returns nil and does not create the user when the authentication fails", %{external_user: external_user} do
      assert nil == Accounts.find_by_token("invalid_auth_token")
      assert_raise Ecto.NoResultsError, fn ->
        Accounts.get_user!(external_user.id)
      end
    end
  end

  describe "user crud" do
    @valid_attrs %{avatar_url: "some avatar_url", email: "some email", id: "7488a646-e31f-11e4-aace-600308960662", name: "some name", role: "member", slug: "some_name"}
    @update_attrs %{avatar_url: "some updated avatar_url", email: "some updated email", id: "7488a646-e31f-11e4-aace-600308960668", name: "some updated name", role: "clubmember", slug: "some_updated_name"}
    @invalid_attrs %{avatar_url: nil, email: nil, id: nil, name: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.avatar_url == "some avatar_url"
      assert user.email == "some email"
      assert user.id == "7488a646-e31f-11e4-aace-600308960662"
      assert user.name == "some name"
      assert user.slug == "some_name"
      assert user.role == "member"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Accounts.update_user(user, @update_attrs)
      assert user.avatar_url == "some updated avatar_url"
      assert user.email == "some updated email"
      assert user.id == "7488a646-e31f-11e4-aace-600308960668"
      assert user.name == "some updated name"
      assert user.slug == "some_updated_name"
      assert user.role == "clubmember"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
