defmodule Grps.GroupsTest do
  use Grps.DataCase
  alias Grps.{Groups, Repo, Discussions}
  alias Grps.Groups.Group

  import Grps.{AccountsFixtures, GroupsFixtures}

  describe "groups" do
    @invalid_attrs %{name: nil, user_id: nil}

    test "list_groups_owned_by/1 returns all groups for just that user" do
      group = group_fixture()
      other_user = Grps.AccountsFixtures.user_fixture()
      _other_group = group_fixture(user_id: other_user.id)
      group = Grps.Repo.get(Group, group.id)

      [%{} = first] = Groups.list_groups_owned_by(group.user_id)

      assert first.id == group.id
      assert first.post_count == 0
      assert first.most_recent == nil
    end

    test "list_groups_owned_by/1 returns all groups for that user ordered by most-recent post" do
      group1 = group_fixture(name: "group1")
      user_id = group1.user_id
      group2 = group_fixture(user_id: user_id, name: "group2")

      {:ok, p1} = Discussions.create_post(%{body: "Foo", group_id: group2.id, user_id: user_id}, user_id)
      {:ok, p2} = Discussions.create_post(%{body: "Foo", group_id: group1.id, user_id: user_id}, user_id)

      [first, second] = Groups.list_groups_owned_by(user_id)

      assert first.id == group1.id
      assert first.post_count == 1
      assert first.most_recent == p2.created_at

      assert second.id == group2.id
      assert second.post_count == 1
      assert second.most_recent == p1.created_at

      {:ok, p3} = Discussions.create_post(%{body: "FooFoo", group_id: group2.id, user_id: user_id}, user_id)

      [first, second] = Groups.list_groups_owned_by(user_id)

      assert first.id == group2.id
      assert first.post_count == 2
      assert first.most_recent == p3.created_at

      assert second.id == group1.id
      assert second.post_count == 1
      assert second.most_recent == p2.created_at
    end

    test "list_groups_containing_member/1 returns all groups where the user is a member" do
      user_id = user_fixture().id
      group1 = create_group_with_member(user_id, %{name: "group1"})
      group2 = create_group_with_member(user_id, %{name: "group2"})
      other = group_fixture(name: "other")

      {:ok, p1} = Discussions.create_post(%{body: "Grp1 Post 1", group_id: group1.id, user_id: group1.user_id}, group1.user_id)
      {:ok, _p2} = Discussions.create_post(%{body: "Grp2 Post 1", group_id: group2.id, user_id: group2.user_id}, group2.user_id)
      {:ok, p3} = Discussions.create_post(%{body: "Grp2 Post 2", group_id: group2.id, user_id: group2.user_id}, group2.user_id)
      {:ok, _p4} = Discussions.create_post(%{body: "Other Post 1", group_id: other.id, user_id: other.user_id}, other.user_id)

      [first, second] = Groups.list_groups_containing_member(user_id)

      assert first.id == group2.id
      assert first.post_count == 2
      assert first.most_recent == p3.created_at

      assert second.id == group1.id
      assert second.post_count == 1
      assert second.most_recent == p1.created_at
    end

    test "get_group!/1 returns the group with given id" do
      group = group_fixture()
      group = Grps.Repo.get(Group, group.id)
      assert Groups.get_group!(group.id) == group
    end

    test "create_group/2 with valid data creates a group" do
      valid_attrs = %{cover_url: "some cover_url", user_id: Grps.AccountsFixtures.user_fixture().id,
        description: "some description", name: "some name"}

      assert {:ok, %Group{} = group} = Groups.create_group(valid_attrs, valid_attrs[:user_id])
      assert group.cover_url == "some cover_url"
      assert group.description == "some description"
      assert group.name == "some name"
      refute group.share_token == nil
    end

    test "create_group/2 with valid data creates valid slug" do
      valid_attrs = %{user_id: Grps.AccountsFixtures.user_fixture().id, name: "Me & You"}

      assert {:ok, %Group{} = group} = Groups.create_group(valid_attrs, valid_attrs[:user_id])

      [prefix, suffix] = String.split(group.slug, "--")
      assert prefix == "me-and-you"

      {suffix_int, ""} = Integer.parse(suffix)
      assert suffix_int > 0 and suffix_int < 1000
    end

    test "create_group/2 with valid data but not owner is not current_user returns error changeset" do
      valid_attrs = %{user_id: Grps.AccountsFixtures.user_fixture().id, name: "Me & You"}
      assert {:error, %Ecto.Changeset{} = changeset} = Groups.create_group(valid_attrs, Ecto.UUID.generate())
      assert changeset.errors[:user_id] == {"Not Authorized!", []}
    end

    test "create_group/2 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Groups.create_group(@invalid_attrs, Ecto.UUID.generate())
    end

    test "update_group/3 with valid data updates the group" do
      group = group_fixture()
      update_attrs = %{cover_url: "some updated cover_url", description: "some updated description", name: "some updated name"}

      assert {:ok, %Group{} = updated_group} = Groups.update_group(group, update_attrs, group.user_id)
      assert updated_group.cover_url == "some updated cover_url"
      assert updated_group.description == "some updated description"
      assert updated_group.name == "some updated name"
      assert updated_group.share_token == group.share_token
      assert updated_group.slug != group.slug
    end

    test "update_group/3 does not change the slug if the name only changes capitalzation" do
      group = group_fixture(name: "original name")
      update_attrs = %{name: "Original Name"}

      assert {:ok, %Group{} = updated_group} = Groups.update_group(group, update_attrs, group.user_id)
      assert updated_group.slug == group.slug
    end

    test "update_group/3 changes the slug if the name changes" do
      group = group_fixture(name: "original name")
      update_attrs = %{name: "New Name"}

      assert {:ok, %Group{} = updated_group} = Groups.update_group(group, update_attrs, group.user_id)
      refute updated_group.slug == group.slug
    end

    test "update_group/3 creates a new share_token if passed as nil" do
      group = group_fixture(name: "original name")
      update_attrs = %{share_token: nil}

      assert {:ok, %Group{} = updated_group} = Groups.update_group(group, update_attrs, group.user_id)
      refute updated_group.share_token == group.share_token
    end

    test "update_group/3 sets delete_by when setting status to deleted" do
      group = group_fixture(name: "original name", delete_by: nil, status: :active)
      update_attrs = %{status: :deleted}

      assert {:ok, %Group{} = updated_group} = Groups.update_group(group, update_attrs, group.user_id)
      assert updated_group.status == :deleted
      refute updated_group.delete_by == nil
    end

    test "update_group/3 clears delete_by when changing status from deleted" do
      group = group_fixture(name: "original name", delete_by: DateTime.utc_now(), status: :deleted)
      update_attrs = %{status: :frozen}

      assert {:ok, %Group{} = updated_group} = Groups.update_group(group, update_attrs, group.user_id)
      assert updated_group.status == :frozen
      assert updated_group.delete_by == nil
    end

    test "update_group/3 with a non-admin current_user returns a changeset" do
      group = group_fixture(name: "original name", delete_by: DateTime.utc_now(), status: :deleted)
      update_attrs = %{status: :frozen}
      assert {:error, :not_authorized} = Groups.update_group(group, update_attrs, Ecto.UUID.generate())
    end

    test "update_group/3 with invalid data returns error changeset" do
      group = group_fixture()
      group = Grps.Repo.get(Group, group.id)
      assert {:error, %Ecto.Changeset{}} = Groups.update_group(group, @invalid_attrs, group.user_id)
      assert group == Groups.get_group!(group.id)
    end

    test "change_group/2 returns a group changeset with no attrs" do
      group = group_fixture()
      assert %Ecto.Changeset{} = Groups.change_group(group)
    end

    test "change_group/2 returns a group changeset with attrs" do
      group = group_fixture()
      assert %Ecto.Changeset{} = Groups.change_group(group, %{body: "foo"})
    end

    test "delete_group/2 deletes a group if user is the owner" do
      group = group_fixture()
      assert {:ok, _group} = Groups.delete_group(group, group.user_id)
    end

    test "delete_group/2 returns an error tuple if not the group owner" do
      group = group_fixture()
      assert {:error, :not_authorized} = Groups.delete_group(group, Ecto.UUID.generate())
    end
  end

  describe "group members" do
    alias Grps.Groups.Membership
    alias Grps.Accounts.User
    alias Grps.Accounts

    @invalid_attrs %{user_id: nil, group_id: nil}

    setup do
      group = group_fixture()
      group = Grps.Repo.get!(Grps.Groups.Group |> preload(:memberships), group.id)
      group_owner = List.first(group.memberships)

      {:ok, group: group, group_owner: group_owner}
    end

    test "list_group_members/1 returns all memberships for the group", %{group: group, group_owner: group_owner} do
      other_user = Grps.AccountsFixtures.user_fixture()
      _other_group = group_fixture(user_id: other_user.id)
      assert Groups.list_group_members(group.id) == [group_owner]
    end

    test "get_group_member!/1 returns the group_member with given id", %{group_owner: group_owner} do
      assert Groups.get_group_member!(group_owner.id) == group_owner
    end

    test "find_group_membership/2 returns the membership if found", %{group: group, group_owner: group_owner} do
      assert Groups.find_group_membership(user_id: group.user_id, group_id: group.id) == group_owner
    end

    test "find_group_membership/2 returns nil if not found", %{group: group} do
      not_user = Grps.AccountsFixtures.user_fixture()
      assert is_nil(Groups.find_group_membership(user_id: not_user.id, group_id: group.id))
    end

    test "find_group_membership/2 returns nil if the user has not accepted the invite yet", %{group: group} do
      invitee = Grps.AccountsFixtures.user_fixture()
      {:ok, %Membership{}} = Groups.invite_group_member(%{user_id: invitee.id, group_id: group.id}, group.user_id)
      assert is_nil(Groups.find_group_membership(user_id: invitee.id, group_id: group.id))
    end

    test "invite_group_member/2 with valid data and owner id creates a group_member and imports the user", %{group: group} do
      user = Grps.AccountsFixtures.user_fixture()
      valid_attrs = %{user_id: user.id, group_id: group.id, group_admin: true, joined_at: ~U[2022-01-13 18:24:00.000000Z], last_visited_at: ~U[2022-01-13 18:24:00.000000Z]}

      assert {:ok, %Membership{} = group_owner} = Groups.invite_group_member(valid_attrs, group.user_id)
      assert group_owner.group_admin == true
      assert group_owner.joined_at == nil
      assert group_owner.last_visited_at == nil

      assert %User{} = Accounts.get_user!(user.id)
    end

    test "invite_group_member/2 with valid data and a non-owner admin creates a group_member and imports the user", %{group: group} do
      other_admin = Grps.AccountsFixtures.user_fixture()
      user = Grps.AccountsFixtures.user_fixture()
      valid_attrs = %{user_id: user.id, group_id: group.id, group_admin: true, joined_at: ~U[2022-01-13 18:24:00.000000Z], last_visited_at: ~U[2022-01-13 18:24:00.000000Z]}
      Repo.insert(Map.merge(%Membership{}, %{valid_attrs | user_id: other_admin.id}))

      assert {:ok, %Membership{} = invitee} = Groups.invite_group_member(valid_attrs, other_admin.id)
      assert invitee.group_admin == true
      assert invitee.joined_at == nil
      assert invitee.last_visited_at == nil

      assert %User{} = Accounts.get_user!(user.id)
    end

    test "invite_group_member/2 with invalid data returns error changeset", %{group: group} do
      assert {:error, %Ecto.Changeset{}} = Groups.invite_group_member(@invalid_attrs, group.user_id)
    end

    test "invite_group_member/2 with valid data but non-admin returns error changeset", %{group: group} do
      user = Grps.AccountsFixtures.user_fixture()
      valid_attrs = %{user_id: user.id, group_id: group.id, group_admin: true, joined_at: ~U[2022-01-13 18:24:00.000000Z], last_visited_at: ~U[2022-01-13 18:24:00.000000Z]}

      assert {:error, :not_authorized} = Groups.invite_group_member(valid_attrs, user.id)
    end

    test "accept_invitation/2 with matching outstanding invite and matching user_id joins the group", %{group: group} do
      user = Grps.AccountsFixtures.user_fixture()
      {:ok, invite} = Groups.invite_group_member(%{group_id: group.id, user_id: user.id}, group.user_id)

      assert {:ok, %Membership{} = member} = Groups.accept_invitation(invite, user.id)
      refute is_nil(member.joined_at)
    end

    test "accept_invitation/2 with matching already completed invite and matching user_id returns an error changeset", %{group: group} do
      user = Grps.AccountsFixtures.user_fixture()
      {:ok, invite} = Repo.insert(%Membership{group_id: group.id, user_id: user.id, joined_at: DateTime.utc_now()})

      assert {:error, %Ecto.Changeset{} = changeset} = Groups.accept_invitation(invite, user.id)
      assert changeset.errors[:user_id] == {"Already Joined", []}
    end

    test "accept_invitation/2 with not matching user_id returns an error changeset", %{group: group} do
      user = Grps.AccountsFixtures.user_fixture()
      {:ok, invite} = Groups.invite_group_member(%{group_id: group.id, user_id: user.id}, group.user_id)

      assert {:error, %Ecto.Changeset{} = changeset} = Groups.accept_invitation(invite, group.user_id)
      assert changeset.errors[:user_id] == {"Not Authorized!", []}
    end

    test "leave_group/2 with the Membership and matching current_user_id deletes the Membership", %{group: group} do
      user = Grps.AccountsFixtures.user_fixture()
      {:ok, membership} = Repo.insert(%Membership{group_id: group.id, user_id: user.id, joined_at: DateTime.utc_now()})

      assert {:ok, %Membership{id: member_id}} = Groups.leave_group(membership, user.id)
      assert member_id == membership.id
      assert Repo.get(Membership, member_id) == nil
    end

    test "leave_group/2 with the Membership and mismatched current_user_id returns an error tuple", %{group: group} do
      user = Grps.AccountsFixtures.user_fixture()
      {:ok, membership} = Repo.insert(%Membership{group_id: group.id, user_id: user.id, joined_at: DateTime.utc_now()})

      assert {:error, :not_authorized} = Groups.leave_group(membership, Ecto.UUID.generate())
      assert Repo.get!(Membership, membership.id) == membership
    end

    test "leave_group/2 with the group owner returns an error tuple", %{group_owner: group_owner} do
      assert {:error, :not_authorized} = Groups.leave_group(group_owner, group_owner.id)
      assert Repo.get!(Membership, group_owner.id) == group_owner
    end

    test "update_group_member/3 with valid data updates the group_member", %{group_owner: group_owner} do
      update_attrs = %{group_admin: false, joined_at: ~U[2022-01-13 18:24:00.000000Z], last_visited_at: ~U[2022-01-13 18:24:00.000000Z]}

      assert {:ok, %Membership{} = group_owner} = Groups.update_group_member(group_owner, update_attrs, group_owner.user_id)
      assert group_owner.group_admin == false
      refute group_owner.joined_at == ~U[2022-01-13 18:24:00.000000Z]
      refute group_owner.last_visited_at == ~U[2022-01-13 18:24:00.000000Z]
    end

    test "update_group_member/3 with valid data but not an admin returns an error changeset", %{group_owner: group_owner} do
      update_attrs = %{group_admin: false, joined_at: ~U[2022-01-13 18:24:00.000000Z], last_visited_at: ~U[2022-01-13 18:24:00.000000Z]}

      assert {:error, :not_authorized} = Groups.update_group_member(group_owner, update_attrs, Ecto.UUID.generate())
    end

    test "update_group_member/3 with invalid data returns error changeset", %{group_owner: group_owner} do
      assert {:error, %Ecto.Changeset{}} = Groups.update_group_member(group_owner, @invalid_attrs, group_owner.user_id)
      assert group_owner == Groups.get_group_member!(group_owner.id)
    end

    test "delete_group_member/1 deletes the group_member", %{group: group, group_owner: group_owner} do
      other_user = Grps.AccountsFixtures.user_fixture()
      {:ok, membership} = Groups.invite_group_member(%{group_id: group.id, user_id: other_user.id}, group.user_id)

      assert {:ok, %Membership{}} = Groups.delete_group_member(membership, group_owner.user_id)
      assert_raise Ecto.NoResultsError, fn -> Groups.get_group_member!(membership.id) end
    end

    test "delete_group_member/1 returns an error tuple when trying to delete the owner's membership", %{group: group, group_owner: group_owner} do
      other_user = Grps.AccountsFixtures.user_fixture()
      {:ok, %Membership{}} = Groups.invite_group_member(%{group_id: group.id, user_id: other_user.id, group_admin: true}, group.user_id)

      assert {:error, :not_authorized} = Groups.delete_group_member(group_owner, other_user.id)
      refute is_nil(Repo.get_by(Membership, %{group_id: group.id, user_id: group_owner.user_id}))
    end

    test "delete_group_member/1 returns an error tuple if the current_user is not a group admin", %{group: group, group_owner: group_owner} do
      other_user = Grps.AccountsFixtures.user_fixture()
      {:ok, %Membership{}} = Groups.invite_group_member(%{group_id: group.id, user_id: other_user.id}, group.user_id)

      assert {:error, :not_authorized} = Groups.delete_group_member(group_owner, other_user.id)
      refute is_nil(Repo.get_by(Membership, %{group_id: group.id, user_id: group_owner.user_id}))
    end
  end
end
