defmodule GrpsWeb.Plugs.UserAuthTest do
  use GrpsWeb.ConnCase, async: true

  alias Grps.Accounts
  alias Grps.Accounts.CoreUser
  alias GrpsWeb.Plugs.UserAuth

  setup %{conn: conn} do
    conn =
      conn
      |> fetch_query_params()
      |> Plug.Test.init_test_session(%{})

    %{user: CoreUser.core_user(), conn: conn}
  end

  describe "log_in_user/2" do
    test "stores the user token in the session", %{conn: conn, user: user} do
      conn = UserAuth.log_in_user(conn, user)
      assert token = get_session(conn, :auth_token)
      assert get_session(conn, :live_socket_id) == "users_sessions:#{Base.url_encode64(token)}"
      assert redirected_to(conn) == "/"
      assert Accounts.find_by_token(token)
    end

    test "clears everything previously stored in the session", %{conn: conn, user: user} do
      conn = conn |> put_session(:to_be_removed, "value") |> UserAuth.log_in_user(user)
      refute get_session(conn, :to_be_removed)
    end

    test "redirects to the configured path", %{conn: conn, user: user} do
      conn = conn |> put_session(:user_return_to, "/hello") |> UserAuth.log_in_user(user)
      assert redirected_to(conn) == "/hello"
    end
  end

  describe "logout_user/1" do
    test "erases session and cookies", %{conn: conn, user: user} do
      auth_token = user.auth_token

      conn =
        conn
        |> put_session(:auth_token, auth_token)
        |> fetch_cookies()
        |> UserAuth.log_out_user()

      refute get_session(conn, :auth_token)
      assert redirected_to(conn) == "/login"
    end

    test "broadcasts to the given live_socket_id", %{conn: conn, user: user} do
      live_socket_id = user.auth_token
      GrpsWeb.Endpoint.subscribe(live_socket_id)

      conn
      |> put_session(:live_socket_id, live_socket_id)
      |> UserAuth.log_out_user()

      assert_receive %Phoenix.Socket.Broadcast{event: "disconnect", topic: ^live_socket_id}
    end

    test "works even if user is already logged out", %{conn: conn} do
      conn = conn |> fetch_cookies() |> UserAuth.log_out_user()
      refute get_session(conn, :auth_token)
      assert redirected_to(conn) == "/login"
    end
  end

  describe "fetch_current_user/2" do
    test "sets current_user from x-forever-authorization header", %{conn: conn, user: user} do
      conn =
        conn
        |> put_req_header("x-forever-authorization", user.auth_token)
        |> UserAuth.fetch_current_user([])

      assert conn.assigns.current_user.id == user.id
    end

    test "sets current_user when auth_token is set in session", %{conn: conn, user: user} do
      conn =
        conn
        |> put_session(:auth_token, user.auth_token)
        |> UserAuth.fetch_current_user()

      assert conn.assigns.current_user.id == user.id
    end

    test "sets current_user when auth_token is set in params", %{user: user} do
      conn =
        build_conn("GET", "example.com", %{auth_token: user.auth_token})
        |> Plug.Test.init_test_session(%{})
        |> UserAuth.fetch_current_user()

      assert conn.assigns.current_user.id == user.id
    end

    test "sets current_user to nil with invalid header", %{conn: conn} do
      conn =
        conn
        |> put_req_header("x-forever-authorization", "junk")
        |> UserAuth.fetch_current_user()

      assert conn.assigns.current_user == nil
    end

    test "sets current_user to nil with invalid session token", %{conn: conn} do
      conn =
        conn
        |> put_session(:auth_token, "invalid_token")
        |> UserAuth.fetch_current_user()

      assert conn.assigns.current_user == nil
    end

    test "sets current_user to nil when auth_token is invalid in params" do
      conn =
        build_conn("GET", "example.com", %{auth_token: "invalid_token"})
        |> Plug.Test.init_test_session(%{})
        |> UserAuth.fetch_current_user()

      assert conn.assigns.current_user == nil
    end
  end

  describe "redirect_if_user_is_authenticated/2" do
    test "redirects if user is authenticated", %{conn: conn, user: user} do
      conn = conn |> assign(:current_user, user) |> UserAuth.redirect_if_user_is_authenticated([])
      assert conn.halted
      assert redirected_to(conn) == "/"
    end

    test "does not redirect if user is not authenticated", %{conn: conn} do
      conn = UserAuth.redirect_if_user_is_authenticated(conn, [])
      refute conn.halted
      refute conn.status
    end
  end

  describe "require_authenticated_user/2" do
    test "redirects if user is not authenticated", %{conn: conn} do
      conn = conn |> fetch_flash() |> UserAuth.require_authenticated_user([])
      assert conn.halted
      assert redirected_to(conn) == Routes.session_path(conn, :new)
      assert get_flash(conn, :error) == "You must log in to access this page."
    end

    test "stores the path to redirect to on GET", %{conn: conn} do
      halted_conn =
        %{conn | path_info: ["foo"], query_string: ""}
        |> fetch_flash()
        |> UserAuth.require_authenticated_user([])

      assert halted_conn.halted
      assert get_session(halted_conn, :user_return_to) == "/foo"

      halted_conn =
        %{conn | path_info: ["foo"], query_string: "bar=baz"}
        |> fetch_flash()
        |> UserAuth.require_authenticated_user([])

      assert halted_conn.halted
      assert get_session(halted_conn, :user_return_to) == "/foo?bar=baz"

      halted_conn =
        %{conn | path_info: ["foo"], query_string: "bar", method: "POST"}
        |> fetch_flash()
        |> UserAuth.require_authenticated_user([])

      assert halted_conn.halted
      refute get_session(halted_conn, :user_return_to)
    end

    test "does not redirect if user is authenticated", %{conn: conn, user: user} do
      conn = conn |> assign(:current_user, user) |> UserAuth.require_authenticated_user([])
      refute conn.halted
      refute conn.status
    end
  end
end
