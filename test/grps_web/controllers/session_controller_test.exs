defmodule GrpsWeb.SessionControllerTest do
  use GrpsWeb.ConnCase, async: true

  alias Grps.Accounts.CoreUser

  setup do
    %{user: CoreUser.core_user()}
  end

  describe "GET /login" do
    test "renders the log in form", %{conn: conn} do
      conn = get(conn, Routes.session_path(conn, :new))
      response = html_response(conn, 200)
      assert response =~ "<h1>Login</h1>"
    end

    test "redirects if already logged in", %{conn: conn, user: user} do
      conn = conn |> log_in_user(user) |> get(Routes.session_path(conn, :new))
      assert redirected_to(conn) == "/"
    end
  end

  describe "POST /users/log_in" do
    test "logs the user in", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.session_path(conn, :create), %{
          "user" => %{"email" => user.email, "password" => "Password1"}
        })

      assert get_session(conn, :auth_token)
      assert redirected_to(conn) == "/"

      # Now do a logged in request and assert on the menu
      conn = get(conn, "/")
      response = html_response(conn, 200)
      assert response =~ user.name
      assert response =~ "Log out</a>"
    end

    test "logs the user in with return to", %{conn: conn, user: user} do
      conn =
        conn
        |> init_test_session(user_return_to: "/foo/bar")
        |> post(Routes.session_path(conn, :create), %{
          "user" => %{
            "email" => user.email,
            "password" => "Password1"
          }
        })

      assert redirected_to(conn) == "/foo/bar"
    end

    test "emits error message with invalid credentials", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.session_path(conn, :create), %{
          "user" => %{"email" => user.email, "password" => "invalid_password"}
        })

      response = html_response(conn, 200)
      assert response =~ "<h1>Login</h1>"
      assert response =~ "Invalid email or password"
    end
  end

  describe "DELETE /users/log_out" do
    test "logs the user out", %{conn: conn, user: user} do
      conn = conn |> log_in_user(user) |> delete(Routes.session_path(conn, :delete))
      assert redirected_to(conn) == "/login"
      refute get_session(conn, :auth_token)
      assert get_flash(conn, :info) =~ "Logged out successfully"
    end

    test "succeeds even if the user is not logged in", %{conn: conn} do
      conn = delete(conn, Routes.session_path(conn, :delete))
      assert redirected_to(conn) == "/login"
      refute get_session(conn, :auth_token)
    end
  end
end
