defmodule GrpsWeb.PageControllerTest do
  use GrpsWeb.ConnCase

  alias Grps.Accounts.CoreUser

  setup %{conn: conn} do
    %{conn: log_in_user(conn, CoreUser.core_user())}
  end

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Forever Groups"
  end
end
