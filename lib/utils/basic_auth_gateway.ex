defmodule Utils.BasicAuthGateway do
  use HTTPoison.Base

  @moduledoc """
  Makes the API request using HTTPoison but adding the Forever Basic Auth credentials and setting longer timeouts.

  Useage:
    `resp = Utils.BasicAuthGateway.get!(url)`

  Returns:
    HTTPoison response
  """

  @doc false
  def process_request_headers(headers) do
    headers ++
    [
      {"Authorization", "Basic #{credentials()}"},
      {"Content-type", "application/json"},
      {"Accept", "application/json"}
    ]
  end

  @doc false
  def process_request_options(options) do
    options ++ [timeout: 15_000, recv_timeout: 15_000]
  end

  defp credentials do
    :base64.encode(
      "#{System.get_env("ADMIN_BASIC_AUTH_NAME")}:#{System.get_env("ADMIN_BASIC_AUTH_PASSWORD")}"
    )
  end
end
