defmodule Utils.YamlLoader do
  @moduledoc false

  def env_from_application_yaml do
    parse_env_yaml()
    |> set_envs()
  end

  defp parse_env_yaml() do
    Path.join(File.cwd!(), "application.yml")
    |> YamlElixir.read_from_file()
  end

  defp set_envs({:ok, %{} = yaml}) do
    Map.get(yaml, env())
    |> Enum.each(&parse_and_set/1)
  end

  defp set_envs(_anything_else), do: "Unable to parse"

  defp env do
    Atom.to_string(Mix.env())
  end

  defp parse_and_set({key, var}) when is_binary(var), do: System.put_env(key, var)

  defp parse_and_set({key, var}) when is_integer(var),
    do: System.put_env(key, Integer.to_string(var))

  defp parse_and_set({key, var}) when is_float(var), do: System.put_env(key, Float.to_string(var))
end
