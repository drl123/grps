defmodule GrpsWeb.SessionController do
  use GrpsWeb, :controller
  alias Grps.Accounts
  alias GrpsWeb.Plugs.UserAuth

  @moduledoc "The Session Controller manages loging in and logging out"

  @doc "Shows the Login form"
  def new(conn, _params) do
    render(conn, "new.html", error_message: nil)
  end

  @doc "Attempts to log the user in"
  def create(conn, %{"user" => user_params}) do
    %{"email" => email, "password" => password} = user_params

    if user = Accounts.authenticate(email, password) do
      UserAuth.log_in_user(conn, user)
    else
      # In order to prevent user enumeration attacks, don't disclose whether the email is registered.
      render(conn, "new.html", error_message: "Invalid email or password")
    end
  end

  @doc "Logs the user out"
  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Logged out successfully.")
    |> UserAuth.log_out_user()
  end
end
