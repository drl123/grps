defmodule GrpsWeb.PageController do
  use GrpsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
