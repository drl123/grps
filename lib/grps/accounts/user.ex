defmodule Grps.Accounts.User do
  use Grps.Schema
  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: false}

  schema "users" do
    field :avatar_url, :string
    field :email, :string
    field :name, :string
    field :role, :string
    field :slug, :string

    has_many :owned_groups, Grps.Groups.Group
    has_many :memberships, Grps.Groups.Membership
    has_many :joined_groups, Grps.Groups.Membership, where: [joined_at: {:not, nil}]
    has_many :open_invitations, Grps.Groups.Membership, where: [joined_at: nil]

    many_to_many :all_groups, Grps.Groups.Group,
                 join_through: Grps.Groups.Membership,
                 join_keys: [user_id: :id, group_id: :id],
                 where: [status: {:in, [:active, :frozen]}]

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:id, :name, :email, :avatar_url, :role, :slug])
    |> validate_required([:id, :name, :email, :role])
    |> unique_constraint(:email)
  end
end
