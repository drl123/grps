defmodule Grps.Accounts.CoreUser.CoreFake do
  alias Grps.Accounts.CoreUser
  @behaviour CoreUser

  @moduledoc "A Fake Core User implementation"

  @core_user %CoreUser{
    auth_token: "valid_auth_token",
    avatar_url: "http://example.com/some_image_url.jpg",
    email: "user@forever.com",
    id: "bc4baccb-af84-4e56-b625-7eb048ffc6ee",
    name: "Mister User",
    role: "member",
    slug: "mister_user"
  }

  @doc false
  @impl CoreUser
  def authenticate("user@forever.com", "Password1") do
    @core_user
  end

  @doc false
  @impl CoreUser
  def authenticate(_email, _password), do: nil

  @doc false
  @impl CoreUser
  def find_by_token("valid_auth_token") do
    @core_user
  end

  @doc false
  @impl CoreUser
  def find_by_token(_auth_token), do: nil

  @doc false
  @impl CoreUser
  def core_user(), do: @core_user

  @doc false
#  @impl CoreUser
  def load_core_user("a-bad-user-id"), do: nil
  def load_core_user(user_id) do
    %CoreUser{
      auth_token: Faker.UUID.v4(),
      avatar_url: Faker.Internet.url(),
      email: Faker.Internet.email(),
      id: user_id,
      name: Faker.Person.name(),
      role: "member",
      slug: Faker.Internet.slug()
    }
  end
end
