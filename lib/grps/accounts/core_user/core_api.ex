defmodule Grps.Accounts.CoreUser.CoreApi do
  alias Grps.Accounts.CoreUser

  @behaviour CoreUser

  @moduledoc "CoreUser implementation using the real Core API"

  # These are unconfigurable constants - if they change, code must change
  @sign_in_path "/api/v3/sign_in"
  @current_user_path "/api/v3/current_user"
  @admin_user_path "/api/v3/admin/users"
  @http_options [{"Content-type", "application/json"}, {"Accept", "application/json"}]

  @impl CoreUser
  @doc false
  def authenticate(email, password) do
    with %{status_code: 200} = resp <- sign_in_to_core_app(email, password),
         %{"user" => %{} = core_user} <- Jason.decode!(resp.body) do
      to_user(core_user)
    else
      _ -> nil
    end
  end

  @doc false
  @impl CoreUser
  def find_by_token(token) do
    with %{status_code: 200} = resp <- get_core_app_current_user(token),
         %{"user" => %{} = core_user} <- Jason.decode!(resp.body) do
      to_user(core_user)
    else
      _ -> nil
    end
  end

  @doc false
  def load_core_user(user_id) do
    with %{status_code: 200} = resp <- load_user_via_admin_api(user_id),
         body <- Jason.decode!(resp.body),
         core_user <- body["user"],
         user <- to_user(core_user) do
      user
    else
      _ -> nil
    end
  end

  @doc false
  def find_core_user(query) do
    with %{status_code: 200} = resp <- find_user_via_admin_api(query),
         body <- Jason.decode!(resp.body),
         %{"deleted_at" => nil} = core_user <- body["user"],
         user <- to_user(core_user) do
      user
    else
      _ -> nil
    end
  end

  @doc false
  @impl CoreUser
  def core_user(), do: %CoreUser{name: nil, email: nil, id: nil, role: nil, slug: nil}

  @doc false
  defp get_core_app_current_user(token) do
    HTTPoison.get!(host() <> @current_user_path, [
      {"X-FOREVER-AUTHORIZATION", token} | @http_options
    ])
  end

  @doc false
  defp sign_in_to_core_app(email, password) do
    params = Jason.encode!(%{email: email, password: password})
    HTTPoison.post!(host() <> @sign_in_path, params, @http_options)
  end

  defp to_user(core_user) do
    %CoreUser{
      id: core_user["id"],
      name: core_user["name"],
      email: core_user["email"],
      role: core_user["role"],
      auth_token: core_user["auth_token"],
      avatar_url: core_user["images"]["thumb"]["url"],
      slug: core_user["slug"]
    }
  end

  defp load_user_via_admin_api(user_id) do
    Utils.BasicAuthGateway.get!(host() <> @admin_user_path <> "/#{user_id}")
  end

  defp find_user_via_admin_api(query_string) do
    params = Jason.encode!(%{:query => query_string})
    Utils.BasicAuthGateway.post!(host() <> @admin_user_path <> "/find", params, @http_options)
  end

  # note: use fn so applies after env loaded
  # || "http://localhost:3000"
  defp host(), do: System.get_env("CORE_API_URL") || "https://core.staging4ever.com"
  def basic_auth_username(), do: System.get_env("ADMIN_BASIC_AUTH_USERNAME")
  def basic_auth_password(), do: System.get_env("ADMIN_BASIC_AUTH_PASSWORD")
end
