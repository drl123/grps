defmodule Grps.Accounts.CoreUser do
  @moduledoc """
    This module manages authenticating and obtaining the Core App User record via API.  It simply
    defines the behaviour and then delegates the actual functionality to either the real API or fake.

    The default implementation calls the actual Core API.  To use the Fake for test or development
    add the following config in the environment:
      `config :grps, :core_user_implementation, Grps.Accounts.CoreUser.CoreFake`
  """

  @implementation Application.get_env(
                    :grps,
                    :core_user_implementation,
                    __MODULE__.CoreApi
                  )

  @type t :: %{
               id: String.t(),
               name: String.t(),
               email: String.t(),
               role: String.t(),
               slug: String.t(),
               avatar_url: String.t() | nil,
               auth_token: String.t() | nil
             }

  @callback authenticate(String.t(), String.t()) :: t() | nil
  @callback find_by_token(String.t()) :: t() | nil
  @callback core_user() :: t()

  @enforce_keys [:id, :email, :name, :role, :slug]
  defstruct [:id, :name, :email, :role, :avatar_url, :auth_token, :slug]

  @doc "Authenticates the user with email and password, returning a CoreUser or nil"
  defdelegate authenticate(email, password), to: @implementation

  @doc "Finds the user by valid auth_token, returning a CoreUser or nil"
  defdelegate find_by_token(token), to: @implementation

  @doc "A convenience method for test that returns the canned CoreUser"
  defdelegate core_user(), to: @implementation

  @doc "Gets the Core User details via Admin User API give user_id"
  defdelegate load_core_user(user_id), to: @implementation
end
