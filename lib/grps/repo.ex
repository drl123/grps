defmodule Grps.Repo do
  use Ecto.Repo,
    otp_app: :grps,
    adapter: Ecto.Adapters.Postgres

  @moduledoc """
    The Repo manages connection to the database.  You can use this file for initializing or setting
    global configuration, etc.
  """
end
