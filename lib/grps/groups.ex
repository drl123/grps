defmodule Grps.Groups do
  @moduledoc """
  Manage listing, creating, updating Groups as well as Group Memberships.

    - Changes to a Group and inviting other Members are restricted to Group Admins.

    - Only the user who was invited may accept their invite to join the Group.

    - The user who created the Group is the Group Owner and is automatically added as a Group Admin.

    - Additonal users may be promoted to Group Admin by another Group Admin.
  """

  import Ecto.Query, warn: false
  import Grps.Groups.{GroupQueries, MembershipQueries}

  alias Grps.Repo
  alias Grps.Groups.{Group, Membership}


  @doc """
  Returns the list of groups owned by that user, sorted by post_count desc.

  ## Examples

      iex> list_groups(user_id)
      [%Group{}, ...]

  """
  def list_groups_owned_by(user_id) do
    from_all_groups()
    |> owned_by(user_id)
    |> order_by_most_recent_post_with_counts()
    |> Repo.all
    |> set_virtual_attrs_in_groups()
  end

  @doc """
  Returns the list of groups that the user is a member of, sorted by post_count desc.

  ## Examples

      iex> list_groups_containing_member(user_id)
      [%Group{}, ...]

  """
  def list_groups_containing_member(user_id) do
    from_all_groups()
    |> active()
    |> order_by_most_recent_post_with_counts()
    |> with_member(user_id)
    |> Repo.all()
    |> set_virtual_attrs_in_groups()
  end

  @doc """
  Gets a single group.

  Raises `Ecto.NoResultsError` if the Group does not exist.

  ## Examples

      iex> get_group!(123)
      %Group{}

      iex> get_group!(456)
      ** (Ecto.NoResultsError)

  """
  def get_group!(id) do
    Repo.get!(Group, id)
  end

  @doc """
  Creates a group, given a name and user_id.
    - Name must be unique within user (owner).
    - User_id must match the current_user id
    - Name and Description min length 3 char.
    - fields:
        - user_id (owner's id)*
        - name (3+ char)*
        - description (3+ char)
        - cover_url

     - automatically assigns the owner as an admin member and sets the joined_at to now (in UTC).
     - automatically sets status to :active

  ## Examples

      iex> create_group(%{user_id: user_id, name: name}, current_user_id)
      {:ok, %Group{}}

      iex> create_group(%{field: bad_value}, current_user_id)
      {:error, %Ecto.Changeset{}}

  """
  def create_group(attrs \\ %{}, current_user_id) do
    %Group{}
    |> Group.creation_changeset(attrs, current_user_id)
    |> Repo.insert()
  end

  @doc """
  Updates a group.
    - Name must be unique within user (group owner).
    - Current user must be a group admin of this group.
    - Name and Description min length 3 char.
    - fields:
        - name (3-50 char)
        - description (3-500 char)
        - status (:active, :frozen, :deleted)
        - cover_url
        - share_token (if set to nil, regnerates, otherwise ignored)

  ## Examples

      iex> update_group(group, %{group_id: id, field: new_value}, current_user_id)
      {:ok, %Group{}}

      iex> update_group(group, %{field: bad_value}, current_user_id)
      {:error, %Ecto.Changeset{}}

      iex> update_group(group, %{group_id: id, field: new_value}, non_admin_user_id)
      {:error, :not_authorized}

  """
  def update_group(%Group{} = group, attrs, current_user_id) do
    if is_group_admin?(group.id, current_user_id) do
      Group.changeset(group, attrs)
      |> Repo.update()
    else
      {:error, :not_authorized}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking group changes. Requires current_user to be
  a group admin.

  ## Examples

      iex> change_group(group, attrs)
      %Ecto.Changeset{data: %Group{}}

  """
  def change_group(%Group{} = group, attrs \\ %{}) do
    Group.changeset(group, attrs)
  end

  @doc """
  Deletes a group (must be owner).

  ## Examples

      iex> delete_group(group_member)
      {:ok, %Membership{}}

      iex> delete_group(group_member)
      {:error, %Ecto.Changeset{}}

      iex> delete_group(group_member)
      {:error, %Ecto.Changeset{}}

  """
  def delete_group(%Group{} = group, current_user_id) do
    if is_group_owner?(group.id, current_user_id) do
      Repo.delete(group)
    else
      {:error, :not_authorized}
    end
  end

  @doc """
  Returns the list of groups_users.

  ## Examples

      iex> list_group_members()
      [%Membership{}, ...]

  """
  def list_group_members(group_id) do
    Membership
    |> for_group(group_id)
    |> Repo.all()
  end

  @doc """
  Gets a single group_member.

  Raises `Ecto.NoResultsError` if the Group member does not exist.

  ## Examples

      iex> get_group_member!(123)
      %Membership{}

      iex> get_group_member!(456)
      ** (Ecto.NoResultsError)

  """
  def get_group_member!(id), do: Repo.get!(Membership, id)

  @doc """
  Finds a Membership by user_id, group_id

  ## Examples
      iex> find_group_membership(user_id: user_id, group_id: group_id)
      %Membership{}

      iex> find_group_membership(user_id: wrong_user_id, group_id: group_id)
      nil
  """
  def find_group_membership(user_id: user_id, group_id: group_id) do
    Membership
    |> for_user(user_id)
    |> for_group(group_id)
    |> accepted()
    |> Repo.one()
  end

  @doc """
  Invites a user to a group as long as the current_user_id passed is in the list of group admins

  ## Examples

      iex> invite_group_member(%{field: value}, current_user_id)
      {:ok, %Membership{}}

      iex> invite_group_member(%{field: bad_value}, current_user_id)
      {:error, %Ecto.Changeset{}}

  """
  def invite_group_member(attrs \\ %{}, current_user_id) do
    if is_group_admin?(attrs[:group_id], current_user_id) do
      %Membership{}
      |> Membership.invite_changeset(attrs)
      |> Repo.insert()
    else
      {:error, :not_authorized}
    end
  end

  @doc """
  Allows a user to accept an invitation.  Only the user who was sent the invite can accept it.

  ## Examples

      iex> accept_invitation(%Membership{user_id: current_user_id}, current_user_id)
      {:ok, %Membership{}}

      iex> accept_invitation(%Membership{user_id: not_current_user_id}, current_user_id)
      {:error, %Ecto.Changeset{}}

  """
  def accept_invitation(%Membership{} = group_member, current_user_id) do
    group_member
    |> Membership.acceptance_changeset(current_user_id)
    |> Repo.update()
  end

  @doc """
  Allows a user to leave a group.
    - Owner cannot leave the group.
    - Passed current_user_id must match Membership ID

  ## Examples

      iex> leave_group(%Membership{user_id: current_user_id}, current_user_id)
      {:ok, %Membership{}}

      iex> leave_group(%Membership{user_id: not_current_user_id}, current_user_id)
      {:error, "Not Authorized!"}

  """
  def leave_group(group_member, current_user_id) do
    group = Repo.get!(Group, group_member.group_id)

    if group_member.user_id == current_user_id && current_user_id != group.user_id do
      Repo.delete(group_member)
    else
      {:error, :not_authorized}
    end
  end

  @doc """
  Updates a group_member as long as the current_user_id is in the list of group admin user IDs.

  ## Examples

      iex> update_group_member(group_member, %{field: new_value}, current_user_id)
      {:ok, %Membership{}}

      iex> update_group_member(group_member, %{field: bad_value}, current_user_id)
      {:error, %Ecto.Changeset{}}

      iex> update_group_member(group_member, %{field: new_value}, not_admin_id)
      {:error, :not_authorized}

  """
  def update_group_member(%Membership{} = group_member, attrs, current_user_id) do
    if is_group_admin?(group_member.group_id, current_user_id) do
      group_member
      |> Membership.changeset(attrs)
      |> Repo.update()
    else
      {:error, :not_authorized}
    end
  end

  @doc """
  Deletes a group_member.

  ## Examples

      iex> delete_group_member(group_member)
      {:ok, %Membership{}}

      iex> delete_group_member(group_member)
      {:error, %Ecto.Changeset{}}

      iex> delete_group_member(group_member)
      {:error, %Ecto.Changeset{}}

  """
  def delete_group_member(%Membership{} = group_member, current_user_id) do
    with %{} <- is_group_admin?(group_member.group_id, current_user_id),
         true <- is_nil(is_group_owner?(group_member.group_id, group_member.user_id))
      do

      Repo.delete(group_member)
    else
      _ -> {:error, :not_authorized}
    end
  end

  defp is_group_admin?(group_id, current_user_id) do
    # if group_id is nil, the changeset will have an error on group_id as required field so let it through here
    is_nil(group_id) || Repo.get_by(Membership, user_id: current_user_id, group_id: group_id, group_admin: true)
  end

  defp is_group_owner?(nil, _user_id), do: nil
  defp is_group_owner?(group_id, user_id), do: Repo.get_by(Group, id: group_id, user_id: user_id)
end
