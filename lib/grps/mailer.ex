defmodule Grps.Mailer do
  use Swoosh.Mailer, otp_app: :grps

  @moduledoc "Wraps Swoosh Mailer if we ever want to add sending emails directly from the app."
end
