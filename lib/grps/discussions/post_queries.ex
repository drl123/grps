defmodule Grps.Discussions.PostQueries do
  import Ecto.Query, warn: false
  alias Grps.Repo
  alias Grps.Discussions.Post

  @moduledoc """
  A set of composable query helpers for managing Posts in Discussions
  """

  @page_size 10

  @doc "limits the query to posts for the given group"
  def for_group(query, group_id), do: where(query, [q], q.group_id == ^group_id)

  @doc "limits the query to toop-level posts for the given group"
  def with_no_parent(query), do: where(query, [q], is_nil(q.parent_id))

  @doc "limits the query children of the parent post"
  def with_parent(query, parent_id), do: where(query, [q], q.parent_id == ^parent_id)

  @doc """
    Applies both the filtering and sorting given the options:
      - around: UUID - post id to center the resultset
      - newer_than: UtcDateTime - filter to posts newer than this date
      - older_than: UtcDateTime - filter to posts older than this date
      - limit: the number of rows to return (defaults to 10)

    The first three are mutually exclusive and will be selected in the priority order shown above.
    Limit can be applied to any sort option (even when no sort)
  """
  def apply_filter_and_sort(query, options) do
    {page_size, opts} = Keyword.pop(options, :limit)
    apply_filter_and_sort(query, opts, page_size || @page_size)
  end

  defp apply_filter_and_sort(query, [around: post_id], page_size) do
    with post <- Repo.get(Post, post_id),
         cursors <- find_cursors(post, page_size)
    do
      query
      |> build_date_where_clause(cursors)
      |> order_by([p], desc: p.created_at)
    else
      _ -> {:error, :not_found}
    end
  end
  defp apply_filter_and_sort(query, [newer_than: datetime], page_size) do
    query
    |> where([q], q.created_at > ^datetime)
    |> order_by([q], desc: q.created_at)
    |> apply_limit(page_size)
  end
  defp apply_filter_and_sort(query, [older_than: datetime], page_size) do
    query
    |> where([q], q.created_at < ^datetime)
    |> order_by([q], desc: q.created_at)
    |> apply_limit(page_size)
  end
  defp apply_filter_and_sort(query, _opts, page_size) do
    order_by(query, [q], desc: q.created_at)
    |> apply_limit(page_size)
  end

  defp apply_limit(query, page_size), do: limit(query, ^page_size)

  #  find_cursors finds the timestamps of the records page_size/2 rows ahead and behind the current record's timestamp
  #  as prev and nxt 'cursors'. This will be used to query for the records surrounding the current record with the
  #  current record centered in the window as much as possible.
  #
  #  If an even page_size is given, the number of rows between the cursors will be page_size + 1.
  #  If an odd page_size, the number of rows between the cursors will equal page size.
  #
  #  If either cursor is nil, that means the selected record is closer to that end of the list than 1/2 the page_size,
  #  so we can leave that clause out in our where clause when selecting the records.  In these cases, the number of rows
  #  may actually be slightly less than page_size.
  #
  def find_cursors(post, page_size) do
    span = div(page_size, 2)
    cursor_query = from p in Post,
                        select: %{id: p.id,
                          created_at: p.created_at,
                          prev: fragment("lag(created_at, ?) over(order by created_at)", ^span),
                          nxt:  fragment("lead(created_at, ?) over(order by created_at)", ^span)},
                        where: p.group_id == ^post.group_id
    Repo.one(from p in subquery(cursor_query), where: p.id == ^post.id)
  end

  defp build_date_where_clause(query, %{nxt: nil, prev: nil}), do: query
  defp build_date_where_clause(query, %{nxt: next_cursor_date, prev: nil}) do
    where(query, [p], p.created_at <= ^next_cursor_date )
  end
  defp build_date_where_clause(query, %{nxt: nil, prev: previous_cursor_date}) do
    where(query, [p], p.created_at >= ^previous_cursor_date)
  end
  defp build_date_where_clause(query, %{nxt: next_cursor_date, prev: previous_cursor_date}) do
    where(query, [p], p.created_at >= ^previous_cursor_date and p.created_at <= ^next_cursor_date )
  end
end
