defmodule Grps.Discussions.Post do
  use Grps.Schema
  import Ecto.Changeset

  schema "posts" do
    field :body, :string
    field :pinned_at, :utc_datetime_usec
    field :pinned_by, :id

    belongs_to :user, Grps.Accounts.User
    belongs_to :group, Grps.Groups.Group
    belongs_to :parent, Grps.Discussions.Post
    has_many :replies, Grps.Discussions.Post, foreign_key: :parent_id

    timestamps()
  end

  @doc false
  def create_changeset(post, attrs) do
    post
    |>changeset(attrs)
    |> cast(attrs, [:user_id, :group_id])
    |> validate_required([:user_id, :group_id])
    |> foreign_key_constraint(:user_id)
    |> foreign_key_constraint(:group_id)
  end

  def changeset(post, attrs) do
    post
    |> cast(attrs, [:body])
    |> validate_required([:body])
    |> validate_length(:body, max: 500)
  end
end
