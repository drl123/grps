defmodule Grps.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    if Mix.env() == :dev do
      Utils.YamlLoader.env_from_application_yaml()
    end

    children = [
      # Start the Ecto repository
      Grps.Repo,
      # Start the Telemetry supervisor
      GrpsWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Grps.PubSub},
      # Start the Endpoint (http/https)
      GrpsWeb.Endpoint,
      # Start a worker by calling: Grps.Worker.start_link(arg)
      # {Grps.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Grps.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    GrpsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
