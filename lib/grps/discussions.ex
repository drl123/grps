defmodule Grps.Discussions do
  @moduledoc """
  The Discussions context.
  """

  import Ecto.Query, warn: false
  import Grps.Discussions.PostQueries

  alias Grps.{Groups, Repo}
  alias Grps.Discussions.{Post}

  @doc """
  Returns the list of posts for the given group.

  Defaults to page size of 10 sorted by created_at desc (newest first)

  Opts is a Keyword List of query options (all optional) as follows:

    - limit: Integer - page size (defaults to 10)
    - older_than: DateTime - filters to posts older than this date, sorted desc
    - newer_than: DateTime - filters to posts newer than this date, sorted asc
    - around: UUID - post id around which to center list

  ## Examples

      iex> list_posts_for_group(some_group_id, limit: 5, newer_than: ~U[2022-01-27 15:19:45.856979Z])
      [%Post{}, ...]

  """
  def list_posts_for_group(group_id, opts \\ []) do
    Post
    |> for_group(group_id)
    |> with_no_parent()
    |> apply_filter_and_sort(opts)
    |> Repo.all()
  end

  @doc """
  Creates a post as long as the user is a group member (has accepted invite).

  Note: returns {:error, :not_authorized} if the user's group membership cannot be
  verified by the attrs[:group_id] & current_user_id, or if the attrs[:user_id] is
  not the same as the current_user_id.

  ## Examples

      iex> create_post(%{field: value, user_id: current_user_id}, current_user_id)
      {:ok, %Post{}}

      iex> create_post(%{field: bad_value, user_id: current_user_id}, current_user_id)
      {:error, %Ecto.Changeset{}}

      iex> create_post(%{field: value, user_id: user_id}, different_user_id)
      {:error, :not_authorized}

      iex> create_post(%{field: value}, non_member_id)
      {:error, :not_authorized}

  """
  def create_post(attrs \\ %{}, current_user_id) do
    with %{} <- Groups.find_group_membership(user_id: current_user_id, group_id: attrs[:group_id]),
         true <- attrs[:user_id] == current_user_id,
         {:ok, post} <- Post.create_changeset(%Post{}, attrs) |> Repo.insert() do
      {:ok, post}
    else
      {:error, changeset} -> {:error, changeset}
      _ -> {:error, :not_authorized}
    end
  end

  @doc """
  Updates a post.

  ## Examples

      iex> update_post(post, %{field: new_value}, current_user_id)
      {:ok, %Post{}}

      iex> update_post(post, %{field: bad_value}, current_user_id)
      {:error, %Ecto.Changeset{}}

  """
  def update_post(%Post{} = post, attrs, current_user_id) do
    with %{} = membership <- Groups.find_group_membership(user_id: current_user_id, group_id: post.group_id),
         true <- post.user_id == current_user_id || membership.group_admin == true,
         {:ok, post} <- Post.changeset(post, attrs) |> Repo.update() do
      {:ok, post}
    else
      {:error, changeset} -> {:error, changeset}
      _ -> {:error, :not_authorized}
    end
  end

  @doc """
  Deletes a post.

  ## Examples

      iex> delete_post(post, current_user_id)
      {:ok, %Post{}}

      iex> delete_post(post, current_user_id)
      {:error, %Ecto.Changeset{}}

  """
  def delete_post(post_id, current_user_id) do
    with {:ok, %Post{}=post} <- load_post(post_id),
         %{} = membership <- Groups.find_group_membership(user_id: current_user_id, group_id: post.group_id),
         true <- post.user_id == current_user_id || membership.group_admin == true,
         {:ok, post} <- post |> Repo.delete() do
      {:ok, post}
    else
      {:error, :not_found} -> {:error, :not_found}
      {:error, changeset} -> {:error, changeset}
      _ -> {:error, :not_authorized}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking post changes.

  ## Examples

      iex> change_post(post)
      %Ecto.Changeset{data: %Post{}}

  """
  def change_post(%Post{} = post, attrs \\ %{}) do
    Post.changeset(post, attrs)
  end

  defp load_post(post_id) do
    case post = Repo.get(Post, post_id) do
      %Post{} -> {:ok, post}
      nil -> {:error, :not_found}
    end
  end
end
