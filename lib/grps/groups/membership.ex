defmodule Grps.Groups.Membership do
  use Grps.Schema
  import Ecto.Changeset
  alias Grps.Accounts.CoreUser
  alias Grps.{Accounts, Groups}

  @moduledoc false

  schema "memberships" do
    field :group_admin, :boolean, default: false
    field :joined_at, :utc_datetime_usec
    field :last_visited_at, :utc_datetime_usec

    belongs_to :user, Accounts.User
    belongs_to :group, Groups.Group

    timestamps()
  end

  @doc false
  def changeset(group_member, attrs \\ %{}) do
    group_member
    |> cast(attrs, [:group_admin, :user_id, :group_id])
    |> validate_required([:user_id, :group_id])
    |> validate_inclusion(:group_admin, [:true, :false])
    |> foreign_key_constraint(:user_id)
    |> foreign_key_constraint(:group_id)
  end

  @doc false
  def invite_changeset(group_member, attrs) do
    group_member
    |> validate_and_import_core_user()
    |> changeset(attrs)
  end

  @doc false
  def acceptance_changeset(group_member, current_user_id) do
    group_member
    |> cast(%{}, [])
    |> validate_user_matches(current_user_id)
    |> accept_if_outstanding()
  end

  defp validate_user_matches(changeset = %{data: %{user_id: current_user_id}}, current_user_id), do: changeset
  defp validate_user_matches(changeset, _not_correct_user_id), do: add_error(changeset, :user_id, "Not Authorized!")

  defp accept_if_outstanding(changeset = %{valid?: false}), do: changeset
  defp accept_if_outstanding(changeset = %{data: %{joined_at: nil}, valid?: true}), do: put_change(changeset, :joined_at, DateTime.utc_now)
  defp accept_if_outstanding(changeset), do: add_error(changeset, :user_id, "Already Joined")

  defp validate_and_import_core_user(changeset = %{changes: %{user_id: user_id}}) do
    with %CoreUser{} = core_user <- CoreUser.load_core_user(user_id) do
      Grps.Accounts.persist_core_user(core_user)
    end
    add_error(changeset, :user_id, "Does not exist")
  end
  defp validate_and_import_core_user(changeset), do: changeset
end
