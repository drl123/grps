defmodule Grps.Groups.MembershipQueries do
  import Ecto.Query, warn: false

  def for_group(query, group_id), do: where(query, group_id: ^group_id)

  def for_user(query, user_id), do: where(query, user_id: ^user_id)

  def accepted(query), do: where(query, [m], not is_nil(m.joined_at))
end
