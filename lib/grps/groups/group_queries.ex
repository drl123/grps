defmodule Grps.Groups.GroupQueries do
  import Ecto.Query, warn: false

  alias Grps.Groups.Group

  @moduledoc false

  def from_all_groups(), do: from(group in Group)

  def order_by_most_recent_post_with_counts(query) do
    from(group in query)
    |> join(:left, [group], p in assoc(group, :posts), as: :posts)
    |> select([group, posts], [group: group, posts: %{most_recent: max(posts.created_at), post_count: count(posts.id)}])
    |> order_by([group, posts], [desc_nulls_last: max(posts.created_at), desc: group.created_at])
    |> group_by([group, posts], group.id)
  end

  def owned_by(query, user_id) do
    where(query, user_id: ^user_id)
  end

  def active(query), do: where(query, [group], group.status != :deleted)

  def with_member(query, user_id) do
    query
    |> join(:left, [group, posts], m in assoc(group, :memberships), as: :membership)
    |> where([group, posts, membership], membership.user_id == ^user_id)
  end

  def set_virtual_attrs_in_groups(results) do
    Enum.map(results, &(Map.merge(&1[:group], &1[:posts])))
  end
end
