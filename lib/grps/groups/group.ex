defmodule Grps.Groups.Group do
  use Grps.Schema
  import Ecto.Changeset
  alias Grps.{Repo, Discussions}

  @moduledoc false

  @groupstauses [:active, :frozen, :deleted]
  @group_deletion_period System.get_env("GROUP_DELETION_DAYS", "60")

  schema "groups" do
    field :cover_url, :string
    field :description, :string
    field :name, :string
    field :share_token, :string
    field :slug, :string
    field :status, Ecto.Enum, values: @groupstauses, default: :active
    field :delete_by, :utc_datetime

    belongs_to :owner, Grps.Accounts.User, foreign_key: :user_id
    has_many :memberships, Grps.Groups.Membership
    many_to_many :members, Grps.Accounts.User,
                 join_through: Grps.Groups.Membership,
                 join_keys: [group_id: :id, user_id: :id]

    has_many :posts, Discussions.Post

    timestamps()
  end

  @doc "used only when creating the group"
  def creation_changeset(new_group, attrs, current_user_id) do
    new_group
    |> changeset(attrs)
    |> cast(attrs, [:user_id])
    |> validate_required([:user_id])
    |> foreign_key_constraint(:user_id)
    |> validate_change(:user_id, fn :user_id, user_id -> if user_id == current_user_id, do: [], else: [user_id: "Not Authorized!"] end)
    |> put_change(:status, :active)
    |> put_assoc(:memberships, [%{user_id: attrs.user_id, group_admin: true, joined_at: DateTime.utc_now}])
  end

  @doc "normal changeset when modifying a group"
  def changeset(group, attrs \\ %{}) do
    group
    |> cast(attrs, [:name, :description, :share_token, :status, :cover_url])
    |> validate_required([:name])
    |> validate_length(:name, min: 3, max: 50)
    |> validate_length(:description, min: 3, max: 500)
    |> unsafe_validate_unique([:name, :user_id], Repo)
    |> unique_constraint([:name, :user_id], name: :groups_user_id_name_index)
    |> validate_inclusion(:status, @groupstauses)
    |> maybe_change_delete_by()
    |> validate_change(:name, fn(_key, name) -> validate_name_has_sluggable_chars(name) end)
    |> maybe_generate_slug()
    |> unique_constraint(:slug)
    |> maybe_set_share_token()
  end

  defp validate_name_has_sluggable_chars(name) do
    if String.length(Slugger.slugify(name)) < 1, do: [name: "Invalid name format"], else: []
  end

  defp maybe_generate_slug(changeset = %{changes: %{name: name}, valid?: true}) do
    new_name = String.downcase(name)
    existing = String.downcase(changeset.data.name || "")
    if new_name != existing, do: generate_slug(changeset), else: changeset
  end
  defp maybe_generate_slug(changeset), do: changeset

  defp generate_slug(changeset = %{changes: %{name: name}}) do
    suffix = :rand.uniform(999)
    prefix = name
             |> String.downcase()
             |> Slugger.slugify()
             |> String.slice(0,50)
    changeset = put_change(changeset, :slug, "#{prefix}--#{suffix}")
    if unsafe_validate_unique(changeset, :slug, Grps.Repo).valid?, do: changeset, else: generate_slug(changeset)
  end

  defp maybe_set_share_token(changeset = %{changes: %{share_token: nil}}), do: generate_token(changeset)
  defp maybe_set_share_token(changeset = %{data: %{share_token: share_token}}) when is_binary(share_token), do: changeset |> delete_change(:share_token)
  defp maybe_set_share_token(changeset), do: generate_token(changeset)

  defp generate_token(changeset), do: put_change(changeset, :share_token, Nanoid.generate())

  defp maybe_change_delete_by(changeset = %{changes: %{status: :deleted}}) do
    put_change(changeset, :delete_by, date_to_delete())
  end
  defp maybe_change_delete_by(changeset), do: put_change(changeset, :delete_by, nil)

  defp date_to_delete() do
    DateTime.utc_now()
    |> DateTime.truncate(:second)
    |> DateTime.add(String.to_integer(@group_deletion_period)*24*60*60)
  end
end
