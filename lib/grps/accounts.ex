defmodule Grps.Accounts do
  @moduledoc """
  The `Accounts` context manages users of the system.  It authenticates the users against the CoreApp and when
  authentication is successful, returns the User with auth_token.
  """

  alias Grps.Accounts.{CoreUser, User}
  alias Grps.Repo

  @doc """
  Authenticates a user against the CoreApp using email and password.

  RETURNS
    - `%CoreUser{name: "user name", email: "user email", avatar_url: "user avatar url", auth_token: "auth-token"}` when successful
    - `nil` when unsuccessful
  """
  def authenticate(email, password) do
    with %CoreUser{} = core_user <- CoreUser.authenticate(email, password),
         {:ok, %User{}} <- persist_core_user(core_user) do
      core_user
    else
      _ -> nil
    end
  end

  @doc """
  Looks up the user in the CoreApp by their auth_token.

  RETURNS
    - `%CoreUser{name: "user name", email: "user email", avatar_url: "user avatar url", auth_token: "auth-token"}` when successful
    - `nil` when unsuccessful
  """
  def find_by_token(token) do
    with %CoreUser{} = core_user <- CoreUser.find_by_token(token),
         {:ok, %User{}} <- persist_core_user(core_user) do
      core_user
    else
      _ -> nil
    end
  end

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert(on_conflict: :replace_all, conflict_target: :id)
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  def persist_core_user(core_user) do
    User.changeset(%User{}, Map.from_struct(core_user))
    |> Repo.insert(
         on_conflict: :replace_all,
         conflict_target: :id,
         returning: true
       )
  end
end
