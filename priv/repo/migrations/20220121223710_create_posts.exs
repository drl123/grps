defmodule Grps.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :body, :text
      add :pinned_at, :utc_datetime_usec
      add :user_id, references(:users, on_delete: :delete_all)
      add :group_id, references(:groups, on_delete: :delete_all)
      add :parent_id, references(:posts, on_delete: :delete_all)
      add :pinned_by, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:posts, [:user_id])
    create index(:posts, [:group_id])
    create index(:posts, [:group_id, :user_id])
    create index(:posts, [:parent_id])
    create index(:posts, [:pinned_by])
  end
end
