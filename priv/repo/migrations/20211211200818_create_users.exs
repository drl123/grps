defmodule Grps.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :email, :string
      add :role, :string
      add :slug, :string
      add :avatar_url, :string

      timestamps()
    end

    create index("users", [:name])
    create unique_index("users", [:email])
  end
end
