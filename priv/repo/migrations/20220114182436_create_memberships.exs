defmodule Grps.Repo.Migrations.CreateMemberships do
  use Ecto.Migration

  def change do
    create table(:memberships) do
      add :group_admin, :boolean, default: false, null: false
      add :joined_at, :utc_datetime_usec
      add :last_visited_at, :utc_datetime_usec
      add :user_id, references(:users, on_delete: :delete_all)
      add :group_id, references(:groups, on_delete: :delete_all)

      timestamps()
    end

    create index(:memberships, [:user_id])
    create index(:memberships, [:group_id])
    create unique_index(:memberships, [:user_id, :group_id])
  end
end
