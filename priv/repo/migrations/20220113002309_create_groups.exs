defmodule Grps.Repo.Migrations.CreateGroups do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS citext"
    create table(:groups) do
      add :user_id, references(:users, on_delete: :delete_all)
      add :name, :citext
      add :description, :text
      add :cover_url, :string
      add :status, :string, default: "active"
      add :delete_by, :utc_datetime
      add :slug, :string, null: false
      add :share_token, :string, null: false

      timestamps()
    end

    create index(:groups, [:user_id])
    create index(:groups, [:user_id, :status])
    create unique_index(:groups, [:user_id, "lower(name)"], name: :groups_user_id_name_index)
    create unique_index(:groups, [:slug])
  end
end
