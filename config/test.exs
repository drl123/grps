import Config
# Use fake implementations in test
config :grps, core_user_implementation: Grps.Accounts.CoreUser.CoreFake

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :grps, Grps.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "grps_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10


# We don't run a server during test. If one is required,
# you can enable the server option below.
config :grps, GrpsWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "FoC7TlYHmtbwBIdVcdanegylCZE/huaW7QOsmky4BcR2oIcZb84OLnUf4ADI1pCK",
  server: false

# In test we don't send emails.
config :grps, Grps.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
