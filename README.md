# Grps (Forever Groups Proof of Concept/Demo)

This system provides the backend for Forever Groups.  It provides both a JSON API and
a WebSocket server(WSS).  The primary interaction will be over WSS, but the API will 
be used to intially create the Groups and list the Groups the user has access to.

For development/demonstration purposes, an HTML front-end is provided that will not 
be needed in Production.  It defaults to using Mars API Staging for authentication
and adding users to Groups.  The demo front-end will allow for local login against
Staging but the UI will then use the JSON API and WSS server as the external clients
will need to do.  The idea is to provide an example of how the clients will interact
with the server.

## Setting up the local environment
You will need to have Erlang, Elixir and Node installed.  This can be done through Homebrew or
via ASDF (recommended).  If using ASDF, install the versions specified in the .tool_versions 
file at the root of the project to ensure you are running the correct versions of each. 

For instance, if the .tool_versions file has:
```bash
elixir 1.13.0-otp-24
erlang 24.1.7
nodejs 16.8.0
```

Then do:
```bash
$asdf plugin add elixir
$asdf plugin add erlang
$asdf plugin add nodejs

$asdf install elixir 1.13.0-otp-24
$asdf install erlang 24.1.7
$asdf install nodejs 16.8.0 
```

Note: please see the asdf plugin notes for each of these three plugins and make sure you
follow all of their prerequisites.  Node in particular, requires the GPG keys to be imported
before installing the plugin. (You may also use NVM for this instead of ASDF if you choose).
Once you have the languages installed, cd'ng into the project directory should automatically
set the proper version for you.

Lastly, copy `application_example.yml` to `application.yml` and fill in the keys.

For CORE_API_URL, you can either use `localhost:3000` or `api.dev.localhost` to hit your local
Mars API server, or just set it to `www.sandbox4ever.com` or `www.staging4ever.com`.  You'll also
need to add the basic auth username and password appropriate for your target environment.

## Starting the Server
To start your grps server the first time or after pulling changes:
  * Install dependencies with `mix deps.get`
  * Install npm assets with `npm install --prefix assets` (you only need this the first time or if package.json changes)
  * Create and migrate your database with:
    * `mix ecto.setup` (first time only) 
    * `mix ecto.migrate` (on subsquent pulls)
  * Start Phoenix endpoint with `mix phx.server` or inside IEx (interactive shell) with `iex -S mix phx.server`
  * Stop it by hitting CTRL-C twice.

Subsequent starting of the server only requires `mix phx.server` or inside IEx with `iex -S mix phx.server`

To run the automated tests, use `mix test`.

To generate the doc's, use `mix docs`.  The doc's will be stored in the `/doc` folder in both HTML and EPUB formats.

Note that no database seeding is required.  Upon Login or when adding Users to a Group, the system
will fetch data from Mars API and then store it locally.

 * Eventually, this will keep in sync via message bus.

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser and log in
with your Staging credentials.

## Dashboard

As long as you are logged in as a Forever Admin, you will be able to visit the Phoenix Dashboard page
from the menu by your avatar.  This will open a diagnostics and metrics page where you can
monitor, debug and manage the running instances in the cluster.  In Production, you will be able
to point the dashboard at any of the connected nodes (servers) in the cluster.
